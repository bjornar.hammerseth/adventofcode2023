use adventofcode2023::day1;
use adventofcode2023::day2;
use criterion::{black_box, criterion_group, criterion_main, Criterion};


// Your function to benchmark
fn benchmark_day1_part1(c: &mut Criterion) {
    c.bench_function("day1_function", |b| b.iter(|| {
        day1::part1();
    }));
}

fn benchmark_day2_part1(c: &mut Criterion) {
    c.bench_function("day2_part1_function", |b| b.iter(|| {
        day2::part1();
    }));
}

fn benchmark_day2_part2(c: &mut Criterion) {
    c.bench_function("day2_part2_function", |b| b.iter(|| {
        day2::part2();
    }));
}

criterion_group!(benches, benchmark_day1_part1, benchmark_day2_part1, benchmark_day2_part2);
criterion_main!(benches);