use std::collections::HashMap;
use std::fs;
use fancy_regex::{Match, Regex};
//This solution is stupid
pub fn part1(){
    let mut numbers: Vec<i32> = vec![];
    let file_path = "input/day_1.txt";

    let contents = fs::read_to_string(file_path)
        .expect("Why could you not read the file, u stoopid?");


    for line in contents.lines() {
        let mut char1 : char = 'F';
        let mut char2 : char = 'F';

        for char in line.chars() {
            if char.is_numeric(){
                if char1 == 'F' {
                    char1 =  char;
                }
                else {
                    char2 =  char;
                }
            }
        }

        if char2 == 'F'{
            char2 = char1
        }

        let combo : String = char1.to_string() + &*char2.to_string();
        let comboint  = combo.parse::<i32>().unwrap();
        numbers.push(comboint)
    }

    let total : i32 = numbers.iter().sum();

}

pub fn part2(){

    let string_to_digit = HashMap::from([("zero", "0"),("one", "1"), ("two", "2"), ("three", "3"), ("four", "4"), ("five", "5"), ("six", "6"), ("seven", "7"), ("eight", "8"), ("nine", "9")]);
    let mut numbers: Vec<i32> = vec![];
    let file_path = "input/day_1.txt";

    let contents = fs::read_to_string(file_path)
        .expect("Why could you not read the file, u stoopid?");
    let re = Regex::new(r"(?=(one|two|three|four|five|six|seven|eight|nine|[0-9]))").unwrap();


    for line in contents.lines() {
        let matches : Vec<Match> = re.find_iter(&line).filter_map(Result::ok).collect();

        println!("{:?}", matches);

        let first_m = matches[0].as_str();
        let last_m = matches[matches.len()-1].as_str();

        let first : &str;
        let second: &str;

        if string_to_digit.contains_key(first_m){
             first = string_to_digit[first_m]
        } else {
            first = first_m;
        }
        if string_to_digit.contains_key(last_m){
            second = string_to_digit[last_m];
        } else {
            second = last_m;
        }


        let number = first.to_owned() + second;
        let my_int: i32 = number.parse::<i32>().unwrap();
        println!("{:?}", my_int);
        numbers.push(my_int);


    }
    let total : i32 = numbers.iter().sum();
    println!("{:?}", total);

}