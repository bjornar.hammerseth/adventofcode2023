use std::collections::HashMap;
use std::fs;
use std::iter::Map;

pub fn part1(){

    let block_colours : Vec<&str> = vec!["green", "blue", "red"];
    let mut in_bag : HashMap<&str, i32> = HashMap::new();
    in_bag.insert("red", 12);
    in_bag.insert("green", 13);
    in_bag.insert("blue", 14);

    let file_path = "input/day_2";
    let mut total = 0;

    let contents = fs::read_to_string(file_path)
        .expect("Why could you not read the file, u stoopid?");

    for line in contents.lines() {
        let vec_line : Vec<&str> = line.split(":").collect();
        let game_id : u32 = vec_line[0].get(5..).unwrap().parse().unwrap();
        let sets : Vec<&str> = vec_line[1].split(";").collect();
        let mut fail = false;

        for set in &sets {
            let colours : Vec<&str> = set.split(",").collect();
            for color in colours {
                let number: Option<i32> = color
                    .split_whitespace()
                    .filter_map(|part| part.parse::<i32>().ok())
                    .next();
                let contained_colour = block_colours.iter().find(|&&colour| color.contains(colour));
                match contained_colour {
                    Some(colour) => {
                        if number.unwrap() > in_bag[colour]{
                            fail = true
                        }
                    },
                    None => println!("No matching colours found in the string"),
                }
            }
        }
        if !fail{
            total += game_id;
        }
    }


}


pub fn part2(){

    let block_colours : Vec<&str> = vec!["green", "blue", "red"];


    let file_path = "input/day_2";
    let mut total = 0;

    let contents = fs::read_to_string(file_path)
        .expect("Why could you not read the file, u stoopid?");

    for line in contents.lines() {
        let mut min_cubes : HashMap<&str, i32> = HashMap::new();
        min_cubes.insert("red", 0);
        min_cubes.insert("green", 0);
        min_cubes.insert("blue", 0);
        let vec_line : Vec<&str> = line.split(":").collect();
        let sets : Vec<&str> = vec_line[1].split(";").collect();

        for set in &sets {
            let colours : Vec<&str> = set.split(",").collect();
            for color in colours {
                let number: Option<i32> = color
                    .split_whitespace()
                    .filter_map(|part| part.parse::<i32>().ok())
                    .next();
                let contained_colour = block_colours.iter().find(|&&colour| color.contains(colour));
                match contained_colour {
                    Some(colour) => {
                        if number.unwrap() > min_cubes[colour] {
                            min_cubes.insert(colour, number.unwrap());
                        }
                    },
                    None => println!("No matching colours found in the string"),
                }
            }
        }
        total = total + min_cubes.values().product::<i32>();
    }
}
